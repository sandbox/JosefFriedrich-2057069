PGN Fundamentals - PGN Grundwissen

# Project page
http://drupal.org/sandbox/JosefFriedrich/2057069

# Git repository
http://drupalcode.org/sandbox/JosefFriedrich/2057069.git

# Translation
- fundamentals: Grundwissen
- subject: Fach (e. g. 'Deutsch', 'Englisch')
- exam_key: KM-Prüfschlüssel
- number: KM-Nummer
- token: Fächer-Kürzel
- name: Fach-Bezeichnung
- grade: Jahrgangsstufe (e. g. '7. Jahrgangsstufe')
- division: Bereich (e. g. 'Sprachlich, geisteswissenschaftlicher Bereich',
  'Gesellschaftswissenschaftlicher Bereich')
