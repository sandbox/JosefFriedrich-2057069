<?php

/**
 * @file
 * Module for fundamentals viewing.
 */

/**
 * @mainpage
 * PGN Fundamentals - PGN Grundwissen
 *
 * @section project_page Project page
 * - @link http://drupal.org/sandbox/JosefFriedrich/2057069 Drupal sandbox project page 2057069 @endlink
 *
 * @section git_repository Git repository
 * - @link http://drupalcode.org/sandbox/JosefFriedrich/2057069.git Drupal sandbox git repository 2057069 @endlink
 *
 * @section translation Translation
 *  - fundamentals: Grundwissen
 *  - subject: Fach (e. g. 'Deutsch', 'Englisch')
 *  - exam_key: KM-Prüfschlüssel
 *  - number: KM-Nummer
 *  - token: Fächer-Kürzel
 *  - name: Fach-Bezeichnung
 *  - grade: Jahrgangsstufe (e. g. '7. Jahrgangsstufe')
 *  - division: Bereich (e. g. 'Sprachlich, geisteswissenschaftlicher Bereich',
 *    'Gesellschaftswissenschaftlicher Bereich')
 */

/**
 * @defgroup drupal_hooks Drupal hooks
 * @{
 * Hooks used by the module.
 */

/**
 * Implements hook_menu().
 */
function pgn_fundamentals_menu() {
  $items = array();

  $menu = array(
    'access callback' => TRUE,
    'file' => 'includes/page.inc',
  );

  $items['grundwissen'] = array(
    'title' => 'Grundwissen',
    'page callback' => 'pgn_fundamentals_overview_subject',
  ) + $menu;

  $items['grundwissen/fach'] = array(
    'title' => 'sortiert nach Fächern',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['grundwissen/jahrgangsstufe'] = array(
    'title' => 'sortiert nach Jahrgangsstufe',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'pgn_fundamentals_overview_grade',
  ) + $menu;

  $items['grundwissen/fach/%pgn_fundamentals_by_exam_key'] = array(
    'title' => 'Grundwissen (bestimmtes Fach)',
    'page callback' => 'pgn_fundamentals_overview',
  ) + $menu;

  $items['grundwissen/jahrgangsstufe/%pgn_fundamentals_by_grade'] = array(
    'title' => 'Grundwissen (Bestimmte Jahrgangsstufe)',
    'page callback' => 'pgn_fundamentals_overview',
  ) + $menu;

  $items['grundwissen/fach/%/%/%pgn_fundamentals_by_exam_key'] = array(
    'title' => 'Fundamentals',
    'title callback' => 'pgn_fundamentals_title_page',
    'title arguments' => array(1, 2),
    'page callback' => 'pgn_fundamentals_pages',
    'page arguments' => array(4, 1),
  ) + $menu;

  $items['grundwissen/jahrgangsstufe/%/%/%pgn_fundamentals_by_grade'] = array(
    'title' => 'grade',
    'title callback' => 'pgn_fundamentals_title_page',
    'title arguments' => array(1, 2),
    'page callback' => 'pgn_fundamentals_pages',
    'page arguments' => array(4, 1),
  ) + $menu;

  return $items;
}

/**
 * Implements hook_node_info().
 */
function pgn_fundamentals_node_info() {
  return array(
    'fundamentals' => array(
      'name' => t('Fundamentals'),
      'module' => 'pgn_fundamentals',
      'description' => t('A node type for fundamentals ordered by subjects and forms.'),
      'has_body' => TRUE,
    ),
  );
}

/**
 * Implements hook_node_presave().
 *
 * Rewrite $node->title to e. g.: 'Fundamentals: Spanisch (12. Jgst.)'
 */
function pgn_fundamentals_node_presave($node) {
  if ($node->type == 'fundamentals') {
    $data = new PgnFundamentalsData();
    $subject = $data->getSubjectByExam($node->exam_key);

    $grade = $node->grade;
    $node->title = t('Fundamentals') . ': ' . $subject . ' (' . $grade . '. Jgst.)';
  }
}

/**
 * Implements hook_node_insert().
 */
function pgn_fundamentals_node_insert($node) {
  if ($node->type == 'fundamentals') {
    drupal_write_record('pgn_fundamentals', $node);
  }
}

/**
 * Implements hook_node_update().
 */
function pgn_fundamentals_node_update($node) {
  if ($node->type == 'fundamentals') {
    drupal_write_record('pgn_fundamentals', $node, 'nid');
  }
}

/**
 * Implements hook_node_delete().
 */
function pgn_fundamentals_node_delete($node) {
  if ($node->type == 'fundamentals') {
    db_delete('pgn_fundamentals')->condition('nid', $node->nid)->execute();
  }
}

/**
 * Implements hook_node_load().
 */
function pgn_fundamentals_node_load($nodes, $modes) {
  foreach ($modes as $mode) {
    if ($mode == 'fundamentals') {
      $result = db_query('SELECT * FROM {pgn_fundamentals} WHERE nid IN(:nids)', array(':nids' => array_keys($nodes)));
      foreach ($result as $record) {
        $nodes[$record->nid]->grade = $record->grade;
        $nodes[$record->nid]->exam_key = $record->exam_key;
      }
    }
  }
}

/**
 * Implements hook_node_view().
 */
function pgn_fundamentals_node_view($node, $view_mode, $langcode) {
  if ($node->type == 'fundamentals' && $view_mode = 'default') {
    module_load_include('inc', 'pgn_fundamentals', 'includes/page');
    $arg = arg();
    if (!empty($arg[0]) && $arg[0] == 'node') {
      $breadcrumb = array();

      if (!empty($node->grade)) {
        $variables = array(
          'grade' => $node->grade,
          'title' => pgn_fundamentals_title('grade', $node->grade),
        );
        $link_grade = theme('pgn_fundamentals_link_grade', $variables);
        $breadcrumb[] = $link_grade;
      }

      if (!empty($node->exam_key)) {
        $variables = array(
          'exam_key' => $node->exam_key,
          'title' => pgn_fundamentals_title('subject', $node->exam_key),
        );
        $link_subject = theme('pgn_fundamentals_link_subject', $variables);
        $breadcrumb[] = $link_subject;
      }
      pgn_fundamentals_breadcrumb($breadcrumb);
    }

    $node->content['pgn_fundamentals_grade'] = array(
      '#theme' => 'pgn_fundamentals_label',
      '#label' => 'Jahrgangsstufe',
      '#content' => $link_grade,
    );

    $node->content['pgn_fundamentals_subject'] = array(
      '#theme' => 'pgn_fundamentals_label',
      '#label' => 'Fach',
      '#content' => $link_subject,
    );

    $content = l(t('Grundwissen geordnet nach Fächer'), 'grundwissen/fach');
    $content .= ' - ';
    $content .= l(t('Grundwissen geordnet nach Jahrgangsstufen'), 'grundwissen/jahrgangsstufe');
    $node->content['pgn_fundamentals_overview'] = array(
      '#theme' => 'pgn_fundamentals_label',
      '#label' => 'Überblick',
      '#content' => $content,
    );
  }
}

/**
 * Implements hook_theme().
 */
function pgn_fundamentals_theme() {
  return array(
    'pgn_fundamentals_link_subject' => array(
      'variables' => array(
        'exam_key' => NULL,
        'title' => NULL,
      ),
      'file' => 'includes/theme.inc',
    ),
    'pgn_fundamentals_link_grade' => array(
      'variables' => array(
        'grade' => NULL,
        'title' => NULL,
      ),
      'file' => 'includes/theme.inc',
    ),
    'pgn_fundamentals_label' => array(
      'variables' => array(
        'label' => NULL,
        'content' => NULL,
      ),
      'file' => 'includes/theme.inc',
    ),
  );
}

/**
 * Implements hook_field_extra_fields().
 */
function pgn_fundamentals_field_extra_fields() {
  $extra['node']['fundamentals'] = array(
    'display' => array(
      'pgn_fundamentals_grade' => array(
        'label' => t('Jahrgangsstufe'),
        'description' => t('Die Jahrgangsstufe anzeigen.'),
        'weight' => 0,
      ),
      'pgn_fundamentals_subject' => array(
        'label' => t('Fach'),
        'description' => t('Fach.'),
        'weight' => 0,
      ),
      'pgn_fundamentals_overview' => array(
        'label' => t('Überblick'),
        'description' => t('Überblick.'),
        'weight' => 0,
      ),
    ),
  );

  return $extra;
}

/**
 * Implements hook_block_info().
 */
function pgn_fundamentals_block_info() {
  $data = new PgnFundamentalsData();
  foreach ($data->getSubjectsKeyedByMode('subject') as $division => $exam_keys) {
    foreach ($exam_keys as $exam_key => $forms) {
      $subject = $data->getSubjectByExam($exam_key);
      $blocks['pgn_fundamentals_subject_' . $exam_key] = array(
        'info' => 'Grundwissen: ' . $subject,
      );
    }
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function pgn_fundamentals_block_view($delta = '') {
  $block = array();
  $exam_key = substr($delta, 25);

  $query = db_query("SELECT nid, grade FROM {pgn_fundamentals} WHERE exam_key = :exam_key ORDER BY grade", array(':exam_key' => $exam_key));
  $records = $query->fetchAll();

  $block['subject'] = 'Grundwissen';
  foreach ($records as $record) {
    $links[] = array(
      'title' => pgn_fundamentals_title('grade', $record->grade),
      'href' => url('node/' . $record->nid, array('absolute' => TRUE)),
    );
  }

  $block['content'] = array(
    '#theme' => 'links',
    '#links' => $links,
  );

  return $block;
}

/**
 * @} End of "defgroup drupal_hooks".
 */

/**
 * @defgroup form_manipulation Form manipulation
 * @{
 * Manipulations on the node form.
 */

/**
 * Implements hook_form().
 *
 * @ingroup drupal_hooks
 */
function pgn_fundamentals_form($node, $form_state) {
  return node_content_form($node, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @ingroup drupal_hooks
 */
function pgn_fundamentals_form_fundamentals_node_form_alter(&$form, &$form_state, $form_id) {
  $node = $form_state['node'];

  unset($form['title']);

  $data = new PgnFundamentalsData();

  $form['grade'] = array(
    '#type' => 'select',
    '#title' => t('Jahrgangsstufe'),
    '#required' => TRUE,
    '#options' => $data->getGrads(),
    '#default_value' => !empty($node->grade) ? $node->grade : 0,
  );

  $form['exam_key'] = array(
    '#type' => 'select',
    '#title' => t('Fach'),
    '#required' => TRUE,
    '#options' => $data->getSubjects(),
    '#default_value' => !empty($node->exam_key) ? $node->exam_key : 0,
  );
}

/**
 * Loads fundamental nodes IDs by exam key.
 *
 * @param int $exam_key
 *   The exam key.
 *
 * @return array
 *   An array of node IDs indexed by nid.
 */
function pgn_fundamentals_by_exam_key_load($exam_key) {
  if (!empty($exam_key)) {
    return pgn_fundamentals_query('subject', $exam_key);
  }
}

/**
 * Loads fundamental node IDs by grade.
 *
 * @param int $grade
 *   The grade.
 *
 * @return array
 *   An array of node IDs indexed by nid.
 */
function pgn_fundamentals_by_grade_load($grade) {
  if (!empty($grade)) {
    return pgn_fundamentals_query('grade', $grade);
  }
}

/**
 * Loads fundamental node IDs.
 *
 * @param string $mode
 *   - subject (Fach)
 *   - grade (Jahrgangsstufe)
 * @param int $value
 *   - grade
 *   - exam_key
 *
 * @return array
 *   An array of node IDs indexed by nid.
 */
function pgn_fundamentals_query($mode, $value) {
  if (!empty($value)) {
    $query = db_select('pgn_fundamentals', 'f');
    $query->join('node', 'n', 'f.nid = n.nid');
    $query->fields('n', array('title'))
      ->fields('f');

    if ($mode == 'subject' || $mode == 'fach') {
      $query->condition('f.exam_key', $value);
      $query->orderBy('f.grade');
    }
    if ($mode == 'grade' || $mode == 'jahrgangsstufe') {
      $query->condition('f.grade', $value);
      $query->orderBy('f.exam_key');
    }

    $nids = $query->execute()
      ->fetchAll();
    return $nids;
  }
}

/**
 * Returns the title of a fundamental.
 *
 * @param string $mode
 *   - subject (Fach)
 *   - grade (Jahrgangsstufe)
 * @param int $value
 *   - grade
 *   - exam_key
 *
 * @return string
 *   String for the title of a fundamental.
 */
function pgn_fundamentals_title($mode, $value) {
  $data = new PgnFundamentalsData();

  if ($mode == 'grade' || $mode == 'jahrgangsstufe') {
    return $value . '. Jahrgangsstufe';
  }
  if ($mode == 'subject' || $mode == 'fach') {
    return $data->getSubjectByExam($value);
  }
}

/**
 * Title callback for the fundamental pages.
 *
 * @param string $mode
 *   - subject (Fach)
 *   - grade (Jahrgangsstufe)
 * @param int $value
 *   - grade
 *   - exam_key
 *
 * @return string
 *   String for the title of the fundamentals pages.
 *
 * @see pgn_fundamentals_menu()
 */
function pgn_fundamentals_title_page($mode, $value) {
  return 'Grundwissen: ' . pgn_fundamentals_title($mode, $value);
}
