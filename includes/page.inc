<?php

/**
 * @file
 * Page callbacks
 */

/**
 * Page callback: Fundamentals overview page.
 *
 * Output:
 *
 * @code
 * Array
 * (
 *     [division_1] => Array
 *         (
 *             [#title] => Sprachlich, geisteswissenschaftlicher Bereich
 *             [#type] => fieldset
 *             [#collapsible] => 1
 *             [#collapsed] =>
 *             [#attached] => Array
 *                 (
 *                     [js] => Array
 *                         (
 *                             [0] => misc/form.js
 *                             [1] => misc/collapse.js
 *                         )
 *
 *                 )
 *
 *             [#attributes] => Array
 *                 (
 *                     [class] => Array
 *                         (
 *                             [0] => collapsible
 *                         )
 *
 *                 )
 *
 *             [subject_1100_heading] => Array
 *                 (
 *                     [#markup] => <a href="/grundwissen/fach/1100/Deutsch ...
 *                 )
 *
 *             [subject_1100_links] => Array
 *                 (
 *                     [#theme] => links
 *                     [#links] => Array
 *                         (
 *                             [0] => Array
 *                                 (
 *                                     [title] => 5. Jahrgangsstufe
 *                                     [href] => http:// ...
 *                                 )
 *                         )
 *                 )
 *         )
 * @endcode
 *
 * @return array
 *   Render array.
 */
function pgn_fundamentals_overview_subject() {
  $data = new PgnFundamentalsData();

  $fieldset = _pgn_fundamentals_fieldset();

  $build = array();
  foreach ($data->getSubjectsKeyedByMode('subject') as $division => $exam_keys) {
    $build['division_' . $division] = array(
      '#title' => $data->getDivisionTitle($division),
    ) + $fieldset;

    foreach ($exam_keys as $exam_key => $forms) {
      $variables = array(
        'exam_key' => $exam_key,
        'title' => pgn_fundamentals_title('subject', $exam_key),
      );
      $build['division_' . $division]['subject_' . $exam_key . '_heading'] = array(
        '#markup' => theme('pgn_fundamentals_link_subject', $variables),
      );

      $links = array();
      foreach ($forms as $grade => $fundamental) {
        $links[] = array(
          'title' => pgn_fundamentals_title('grade', $grade),
          'href' => url('node/' . $fundamental->nid, array('absolute' => TRUE)),
        );
      }

      $build['division_' . $division]['subject_' . $exam_key . '_links'] = array(
        '#theme' => 'links',
        '#links' => $links,
      );
    }
  }

  return $build;
}

/**
 * Page callback: Fundamentals overview page.
 *
 * @return array
 *   Render array.
 */
function pgn_fundamentals_overview_grade() {
  $data = new PgnFundamentalsData();

  $fieldset = _pgn_fundamentals_fieldset();

  $build = array();
  foreach ($data->getSubjectsKeyedByMode('grade') as $grade => $divisions) {

    $key = 'grade_' . $grade;
    $build[$key] = array(
      '#title' => pgn_fundamentals_title('grade', $grade),
    ) + $fieldset;

    foreach ($divisions as $devision_key => $division) {
      $build[$key]['subject_' . $devision_key . '_heading'] = array(
        '#markup' => $data->getDivisionTitle($devision_key),
      );

      $links = array();
      foreach ($division as $exam_key => $fundamental) {
        $links[] = array(
          'title' => pgn_fundamentals_title('subject', $exam_key),
          'href' => url('node/' . $fundamental->nid, array('absolute' => TRUE)),
        );
      }

      $build[$key]['subject_' . $exam_key . '_links'] = array(
        '#theme' => 'links',
        '#links' => $links,
      );
    }
  }

  return $build;
}

/**
 * Page callback: Fundamental pages.
 *
 * @param object $nodes
 *   Nodes objects.
 * @param string $mode
 *   - fach
 *   - jahrgangsstufe
 *
 * @return array
 *   Render array.
 */
function pgn_fundamentals_pages($nodes, $mode) {
  pgn_fundamentals_breadcrumb();

  $build = array();

  $links = array();
  foreach ($nodes as $node) {

    if ($mode == 'fach') {
      $value = $node->grade;
      $title_mode = 'grade';
    }

    if ($mode == 'jahrgangsstufe') {
      $value = $node->exam_key;
      $title_mode = 'subject';
    }

    $link['title'] = pgn_fundamentals_title($title_mode, $value);
    $link['href'] = url('node/' . $node->nid, array('absolute' => TRUE));
    $links[] = $link;
  }

  $build['links'] = array(
    '#theme' => 'links',
    '#links' => $links,
  );
  return $build;
}

/**
 * Shows breadcrumbs for fundamentals.
 *
 * @param array $breadcrumb_adds
 *   An array with links to show as breadcrumbs.
 */
function pgn_fundamentals_breadcrumb($breadcrumb_adds = array()) {
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Grundwissen'), 'grundwissen');
  foreach ($breadcrumb_adds as $breadcrumb_add) {
    $breadcrumb[] = $breadcrumb_add;
  }
  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Returns render array to make a fieldset.
 *
 * Output:
 *
 * @code
 * Array
 * (
 *     [#type] => fieldset
 *     [#collapsible] => 1
 *     [#collapsed] =>
 *     [#attached] => Array
 *         (
 *             [js] => Array
 *                 (
 *                     [0] => misc/form.js
 *                     [1] => misc/collapse.js
 *                 )
 *
 *         )
 *
 *     [#attributes] => Array
 *         (
 *             [class] => Array
 *                 (
 *                     [0] => collapsible
 *                 )
 *
 *         )
 *
 * )
 * @endcode
 *
 * @return array
 *   A render array to make a fieldset.
 */
function _pgn_fundamentals_fieldset() {
  return array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attached' => array(
      'js' => array(
        'misc/form.js',
        'misc/collapse.js',
      ),
    ),
    '#attributes' => array(
      'class' => array('collapsible'),
    ),
  );
}
