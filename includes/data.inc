<?php

/**
 * @file
 * Data functions
 */

class PgnFundamentalsData {

  /**
   * A multidimensional array of subjects.
   *
   * @code
   *   $subjects = array(
   *     'Sprachlich, geisteswissenschaftlicher Bereich' => array(
   *       'D' => array(
   *         'exam_key' => 1100,
   *         'number' => 023,
   *         'token' => 'D',
   *         'name' => 'Deutsch',
   *       ),
   *       'E' => array(
   *         'exam_key' => 1210,
   *         'number' => 026,
   *         'token' => 'E',
   *         'name' => 'Englisch',
   *        ),
   *      ),
   *    );
   * @endcode
   */
  public $subjects = array();

  /**
   * Constructor function.
   */
  public function __construct() {
    $this->subjects = array(
      'Sprachlich, geisteswissenschaftlicher Bereich' => array(
        'D' => array(
          'exam_key' => 1100,
          'number' => 023,
          'token' => 'D',
          'name' => 'Deutsch',
        ),
        'E' => array(
          'exam_key' => 1210,
          'number' => 026,
          'token' => 'E',
          'name' => 'Englisch',
        ),
        'F' => array(
          'exam_key' => 1220,
          'number' => 027,
          'token' => 'F',
          'name' => 'Französisch',
        ),
        'L' => array(
          'exam_key' => 1240,
          'number' => '024',
          'token' => 'L',
          'name' => 'Latein',
        ),
        'SP' => array(
          'exam_key' => 1270,
          'number' => '029',
          'token' => 'SP',
          'name' => 'Spanisch',
        ),
        'KU' => array(
          'exam_key' => 1310,
          'number' => '048',
          'token' => 'KU',
          'name' => 'Kunst',
        ),
        'MU' => array(
          'exam_key' => 1320,
          'number' => '047',
          'token' => 'MU',
          'name' => 'Musik',
        ),
        'CHI' => array(
          'exam_key' => 1418,
          'number' => '616',
          'token' => 'CHI',
          'name' => 'Chinesisch',
        ),
        'TÜ' => array(
          'exam_key' => 1422,
          'number' => '083',
          'token' => 'TÜ',
          'name' => 'Türkisch',
        ),
      ),
      'Gesellschaftswissenschaftlicher Bereich' => array(
        'G' => array(
          'exam_key' => 2110,
          'number' => '037',
          'token' => 'G',
          'name' => 'Geschichte',
        ),
        'SK' => array(
          'exam_key' => 2120,
          'number' => '044',
          'token' => 'SK',
          'name' => 'Sozialkunde',
        ),
        'GEO' => array(
          'exam_key' => 2210,
          'number' => '083',
          'token' => 'GEO',
          'name' => 'Geographie',
        ),
        'WR' => array(
          'exam_key' => 2230,
          'number' => '043',
          'token' => 'WR',
          'name' => 'Wirtschaft und Recht',
        ),
        'EV' => array(
          'exam_key' => 2310,
          'number' => '022',
          'token' => 'EV',
          'name' => 'Ev. Religionslehre',
        ),
        'K' => array(
          'exam_key' => 2320,
          'number' => '021',
          'token' => 'K',
          'name' => 'Kath. Religionslehre',
        ),
        'ETH' => array(
          'exam_key' => 2360,
          'number' => '059',
          'token' => 'ETH',
          'name' => 'Ethik',
        ),
      ),
      'Mathematisch, naturwissenschaftlicher Bereich' => array(
        'M' => array(
          'exam_key' => 3100,
          'number' => '039',
          'token' => 'M',
          'name' => 'Mathematik',
        ),
        'B' => array(
          'exam_key' => 3210,
          'number' => '042',
          'token' => 'B',
          'name' => 'Biologie',
        ),
        'C' => array(
          'exam_key' => 3220,
          'number' => '041',
          'token' => 'C',
          'name' => 'Chemie',
        ),
        'PH' => array(
          'exam_key' => 3230,
          'number' => '040',
          'token' => 'PH',
          'name' => 'Physik',
        ),
        'NT' => array(
          'exam_key' => 3234,
          'number' => '040',
          'token' => 'NT',
          'name' => 'Natur und Technik',
        ),
        'INF' => array(
          'exam_key' => 3505,
          'number' => '861',
          'token' => 'INF',
          'name' => 'Informatik',
        ),
      ),
      'Sport' => array(
        'SPO' => array(
          'exam_key' => 4100,
          'number' => '999',
          'token' => 'SPO',
          'name' => 'Sport',
        ),
      ),
    );
  }

  /**
   * Returns an array for the grade selection form element.
   *
   * @code
   * Array
   * (
   *     [5] => 5. Jahrgangsstufe
   *     [6] => 6. Jahrgangsstufe
   *     [7] => 7. Jahrgangsstufe
   *     [8] => 8. Jahrgangsstufe
   *     [9] => 9. Jahrgangsstufe
   *     [10] => 10. Jahrgangsstufe
   *     [11] => 11. Jahrgangsstufe
   *     [12] => 12. Jahrgangsstufe
   * )
   * @endcode
   *
   * @return array
   *   An array of grade titels keyed by the grade number.
   */
  public function getGrads() {
    $grades = array();
    for ($i = 5; $i <= 12; $i++) {
      $grades[$i] = $i .  '. Jahrgangsstufe';
    }

    return $grades;
  }

  /**
   * Returns a mulidimensional array for a select form element.
   *
   * Output:
   *
   * @code
   * Array
   * (
   *     [Sprachlich, geisteswissenschaftlicher Bereich] => Array
   *         (
   *             [1100] => Deutsch
   *             [1210] => Englisch
   *             [1220] => Französisch
   *             [1240] => Latein
   *             [1270] => Spanisch
   *             [1310] => Kunst
   *             [1320] => Musik
   *             [1418] => Chinesisch
   *             [1422] => Türkisch
   *         )
   *
   *     [Gesellschaftswissenschaftlicher Bereich] => Array
   *         (
   *             [2110] => Geschichte
   *             [2120] => Sozialkunde
   *             [2210] => Geographie
   *             [2230] => Wirtschaft und Recht
   *             [2310] => Ev. Religionslehre
   *             [2320] => Kath. Religionslehre
   *             [2360] => Ethik
   *         )
   *
   *     [Mathematisch, naturwissenschaftlicher Bereich] => Array
   *         (
   *             [3100] => Mathematik
   *             [3210] => Biologie
   *             [3220] => Chemie
   *             [3230] => Physik
   *             [3234] => Natur und Technik
   *             [3505] => Informatik
   *         )
   *
   *     [Sport] => Array
   *         (
   *             [4100] => Sport
   *         )
   *
   * )
   * @endcode
   *
   * @return array
   *   A array used for the node form.
   */
  public function getSubjects() {
    foreach ($this->subjects as $division => $divisions) {
      foreach ($divisions as $subject) {
        $output[$division][$subject['exam_key']] = $subject['name'];
      }
    }

    return $output;
  }

  /**
   * Returns the subject.
   *
   * @param int $exam_key
   *   The exam key.
   *
   * @return string
   *   The subject.
   */
  public function getSubjectByExam($exam_key) {
    $subjects = $this->flipSubjects();
    return $subjects[$exam_key];
  }

  /**
   * Return a array of subject names keyed by exam_key.
   *
   * @code
   * Array
   * (
   *     [1100] => Deutsch
   *     [1210] => Englisch
   *     [1220] => Französisch
   *     [1240] => Latein
   *     [1270] => Spanisch
   *     [1310] => Kunst
   *     [1320] => Musik
   *     [1418] => Chinesisch
   *     [1422] => Türkisch
   *     [2110] => Geschichte
   *     [2120] => Sozialkunde
   *     [2210] => Geographie
   *     [2230] => Wirtschaft und Recht
   *     [2310] => Ev. Religionslehre
   *     [2320] => Kath. Religionslehre
   *     [2360] => Ethik
   *     [3100] => Mathematik
   *     [3210] => Biologie
   *     [3220] => Chemie
   *     [3230] => Physik
   *     [3234] => Natur und Technik
   *     [3505] => Informatik
   *     [4100] => Sport
   * )
   * @endcode
   *
   * @param bool $reverse
   *   Booleon whether returned array should get to array_flip().
   *
   * @return array
   *   A array of subject names keyed by exam_key.
   */
  public function flipSubjects($reverse = FALSE) {
    foreach ($this->subjects as $divisions) {
      foreach ($divisions as $subjects) {
        $output[$subjects['exam_key']] = $subjects['name'];
      }
    }

    if ($reverse) {
      array_flip($output);
    }

    return $output;
  }

  /**
   * Return a array of divisions keyed by the first character of exam_key.
   *
   * @code
   * Array
   * (
   *     [1] => Sprachlich, geisteswissenschaftlicher Bereich
   *     [2] => Gesellschaftswissenschaftlicher Bereich
   *     [3] => Mathematisch, naturwissenschaftlicher Bereich
   *     [4] => Sport
   * )
   * @endcode
   *
   * @return array
   *   Array of divisions.
   */
  public function getDivisions() {
    $divisions = array_keys($this->subjects);
    $output = array();
    $count = 1;
    foreach ($divisions as $division) {
      $output[$count] = $division;
      $count++;
    }

    return $output;
  }

  /**
   * Returns the title of a division.
   *
   * @param int $number
   *   The division number.
   *
   * @return string
   *   The title of a division.
   */
  public function getDivisionTitle($number) {
    $divisions = $this->getDivisions($number);
    return $divisions[$number];
  }

  /**
   * Returns a sorted array to build the fundamentals overview pages.
   *
   * Output by mode "subject":
   *
   * @code
   * Array
   * (
   *     [1] => Array
   *         (
   *             [1100] => Array
   *                 (
   *                     [5] => stdClass Object
   *                         (
   *                             [nid] => 9965
   *                             [exam_key] => 1100
   *                             [grade] => 5
   *                         )
   *
   *                     [6] => stdClass Object
   *                         (
   *                             [nid] => 10000
   *                             [exam_key] => 1100
   *                             [grade] => 6
   *                         )
   * @endcode
   *
   * Output by mode "grade":
   *
   * @code
   * Array
   * (
   *     [5] => Array
   *         (
   *             [1] => Array
   *                 (
   *                     [1100] => stdClass Object
   *                         (
   *                             [nid] => 9965
   *                             [exam_key] => 1100
   *                             [grade] => 5
   *                         )
   *
   *                     [1210] => stdClass Object
   *                         (
   *                             [nid] => 10051
   *                             [exam_key] => 1210
   *                             [grade] => 5
   *                         )
   * @endcode
   *
   * @param string $mode
   *   The two modes are:
   *   - subject
   *   - grade
   *
   * @return array
   *   A array tree to build the overview pages.
   */
  public function getSubjectsKeyedByMode($mode = 'subject') {
    $query = db_select('pgn_fundamentals', 'f')
      ->fields('f');

    if ($mode == 'subject') {
      $query->orderBy('exam_key')
        ->orderBy('grade');
    }
    else {
      // Mode is grade.
      $query->orderBy('grade')
        ->orderBy('exam_key');
    }

    $fundamentals = $query->execute()->fetchAll();

    $tree = array();
    foreach ($fundamentals as $fundamental) {
      if (!empty($fundamental->exam_key) && !empty($fundamental->grade)) {
        $division_key = substr($fundamental->exam_key, 0, 1);
        if ($mode == 'subject') {
          $tree[$division_key][$fundamental->exam_key][$fundamental->grade] = $fundamental;
        }
        else {
          // Mode is grade.
          $tree[$fundamental->grade][$division_key][$fundamental->exam_key] = $fundamental;
        }

      }
    }
    return $tree;
  }

}
