<?php

/**
 * @file
 * Theme functions
 */

/**
 * Retrieves a HTML link to a subject page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - exam_key: The exam key.
 *   - title: A HTML link to a subject page.
 *
 * @ingroup themeable
 */
function theme_pgn_fundamentals_link_subject($variables) {
  $exam_key = $variables['exam_key'];
  $title = $variables['title'];

  $path = 'grundwissen/fach/' . $exam_key . '/' . $title . '/' . $exam_key;
  return l($title, $path);
}

/**
 * Retrieves a HTML link to a grade page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - grade: The grade.
 *
 * @ingroup themeable
 */
function theme_pgn_fundamentals_link_grade($variables) {
  $grade = $variables['grade'];
  $title = $variables['title'];

  $path = 'grundwissen/jahrgangsstufe/' . $grade . '/';
  $path .= $grade . '-jahrgangsstufe/' . $grade;
  return l($title, $path);
}

/**
 * Shows to a given content a themed label.
 *
 * @param array $variables
 *   An associative array containing:
 *   - label: The label.
 *   - content: The conten.
 *
 * @ingroup themeable
 */
function theme_pgn_fundamentals_label($variables) {
  $label = $variables['label'];
  $content = $variables['content'];

  $output = '<section class="pgn-fundamentals">';
  $output .= '<strong>' . $label . ':</strong> ';
  $output .= $content;
  $output .= '</section>';
  return $output;
}
